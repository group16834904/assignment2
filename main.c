#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                break;
            case 4:
                printf("Enter key to delete: ");
                scanf("%d", &key);
                deleteByKey(&head, key);
                break;
            case 5:
                printf("Enter value to delete: ");
                scanf("%d", &data);
                deleteByValue(&head, data);
                break;
            case 6:
                printf("Enter key to insert after: ");
                scanf("%d", &key);
                printf("Enter value to insert: ");
                scanf("%d", &data);
                insertAfterKey(&head, key, data);
                break;
            case 7:
                printf("Enter value to search: ");
                scanf("%d", &data);
                printf("Enter value to insert: ");
                scanf("%d", &key);
                insertAfterValue(&head, data, key);
                break;
            case 8:
                printf("Exiting program.\n");
                exit(0);
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

// Function definitions
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Memory allocation failed.\n");
        exit(1);
    }
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *currentNode = *head;
    while (currentNode->next != NULL)
    {
        currentNode = currentNode->next;
    }
    currentNode->next = newNode;
}

void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
    if (*head == NULL)
    {
        printf("List is empty.\n");
        return;
    }
    struct Node *currentNode = *head;
    struct Node *prevNode = NULL;
    if (currentNode != NULL && currentNode->number == key)
    {
        *head = currentNode->next;
        free(currentNode);
        return;
    }
    while (currentNode != NULL && currentNode->number != key)
    {
        prevNode = currentNode;
        currentNode = currentNode->next;
    }
    if (currentNode == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    prevNode->next = currentNode->next;
    free(currentNode);
}

void deleteByValue(struct Node **head, int value)
{
    struct Node *currentNode = *head;
    struct Node *prevNode = NULL;
    if (currentNode != NULL && currentNode->number == value)
    {
        *head = currentNode->next;
        free(currentNode);
        return;
    }
    while (currentNode != NULL && currentNode->number != value)
    {
        prevNode = currentNode;
        currentNode = currentNode->next;
    }
    if (currentNode == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    prevNode->next = currentNode->next;
    free(currentNode);
}

void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *currentNode = *head;
    while (currentNode != NULL && currentNode->number != key)
    {
        currentNode = currentNode->next;
    }
    if (currentNode == NULL)
    {
        printf("Key not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = currentNode->next;
    currentNode->next = newNode;
}

void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *currentNode = *head;
    while (currentNode != NULL && currentNode->number != searchValue)
    {
        currentNode = currentNode->next;
    }
    if (currentNode == NULL)
    {
        printf("Value not found in the list.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = currentNode->next;
    currentNode->next = newNode;
}
